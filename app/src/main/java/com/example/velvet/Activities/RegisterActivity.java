package com.example.velvet.Activities;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.util.Patterns;
import android.view.View;
import android.view.textclassifier.TextLinks;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.StringRequest;
import com.example.velvet.R;
import com.example.velvet.Utils.Endpoints;
import com.example.velvet.Utils.VolleySingleton;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class RegisterActivity extends AppCompatActivity {

private EditText nameEt,emailEt,usernameEt,passwordEt,numberEt,cityEt,sizeEt;
private Button submitButton;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);
        nameEt = findViewById(R.id.name);
        emailEt = findViewById(R.id.email);
        usernameEt = findViewById(R.id.username);
        passwordEt = findViewById(R.id.password);
        numberEt = findViewById(R.id.number);
        cityEt = findViewById(R.id.city);
        sizeEt = findViewById(R.id.size);
        submitButton = findViewById(R.id.submit_button);
        submitButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String name, email, username, password, number, city, size;
                name = nameEt.getText().toString();
                email = emailEt.getText().toString();
                username = usernameEt.getText().toString();
                password = passwordEt.getText().toString();
                number = numberEt.getText().toString();
                city = cityEt.getText().toString();
                size = sizeEt.getText().toString();
                if (isValid(name, email, username, password, number, city, size)) {
                    register(name, email, username, password, number, city, size);
                }
                //showMessage(name+"\n"+email+"\n"+username+"\n"+password+"\n"+number+"\n"+city+"\n"+size+"\n");

            }
        });
    }
    private void register(final String name, final String email, final String username, final String password, final String number, final String city, final String size){
           StringRequest stringRequest = new StringRequest(Request.Method.POST, Endpoints.register_url, new Response.Listener<String>() {
               @Override
               public void onResponse(String response) {
               if(response.equals("Success"))
                   {
                       Toast.makeText(RegisterActivity.this, response, Toast.LENGTH_SHORT).show();
                       startActivity(new Intent(RegisterActivity.this, MainActivity.class));
                       RegisterActivity.this.finish();
                   }else {
                       Toast.makeText(RegisterActivity.this, response, Toast.LENGTH_SHORT).show();
                   }
               }
           }, new Response.ErrorListener() {
               @Override
               public void onErrorResponse(VolleyError error) {
                   Toast.makeText(RegisterActivity.this, "Something went wrong", Toast.LENGTH_SHORT).show();
                   Log.d("VOLLEY", error.getMessage());
               }
           }) {
               @Override
               protected Map<String, String> getParams() throws AuthFailureError {
                   Map<String,String> params = new HashMap<>();
                   params.put("name", name);
                   params.put("email", email);
                   params.put("username", username);
                   params.put("password", password);
                   params.put("number", number);
                   params.put("city", city);
                   params.put("size", size);




                   return params;

               }
           };
            VolleySingleton.getInstance(this).addToRequestQueue(stringRequest);
        }

    private boolean isValid(String name, String email, String username,String password, String number, String city, String size)
    {
        List<String> valid_sizes = new ArrayList<>();
        valid_sizes.add("XXS");
        valid_sizes.add("XS");
        valid_sizes.add("S");
        valid_sizes.add("M");
        valid_sizes.add("L");
        valid_sizes.add("XL");
        valid_sizes.add("XXL");
        if(name.isEmpty()) {
            showMessage("Full name is empty!");
            return false;
        }else if(email.isEmpty() && android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches()){
            showMessage("Email is invalid!");
            return false;
        }else if (username.isEmpty()){
            showMessage("Username is empty!");
            return false;
        }else if (password.isEmpty()){
            showMessage("Password is empty!");
            return false;
        }else if (number.length() !=9){
            showMessage("Invalid mobile number, it should contain only 9 digits!");
            return false;
        }else if (city.isEmpty()) {
            showMessage("City is empty!");
            return false;
        }else if (!valid_sizes.contains(size)) {
            showMessage("Size invalid! Please choose from" +valid_sizes);
            return false;
        }

       return true;
    }




    private void showMessage(String msg) {
        Toast.makeText(this, msg, Toast.LENGTH_SHORT).show();
    }
}
