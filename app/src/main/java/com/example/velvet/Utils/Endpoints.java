package com.example.velvet.Utils;

public class Endpoints {
     private static final String  base_url = "https://velvetapp.xyz/";

     public static final String register_url = base_url+ "register.php";
     public static final String login_url = base_url+ "login.php";
     public static final String upload_request = base_url+"upload_request.php";
     public static final String get_requests = base_url+"get_requests.php";
}
